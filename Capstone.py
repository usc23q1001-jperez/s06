from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod

    def getFullName(self):
        pass
    
    def addRequest(self):
        pass
    
    def checkRequest(self):
        pass

    def addUser(self):
        pass

class Employee(Person):

    def __init__(self, fname, lname, email, dept):
        
        super().__init__()
        self._fname = fname
        self._lname = lname
        self._email = email
        self._dept = dept
    
    def getFname(self):
        print(f'The first name of employee is {self._fname}')
        
    def getLname(self):
        print(f'The last name of employee is {self._lname}')

    def getEmail(self):
        print(f'The email of employee is {self._email}')

    def getDept(self):
        print(f'The department of employee is {self._dept}')

    def setLname(self, lname):
        self._lname = lname
    
    def setFname(self, fname):
        self._fname = fname

    def setEmail(self, email):
        self._email = email
    
    def setDept(self, dept):
        self._dept = dept

    def getFullName(self):
        return f"{self._fname} {self._lname}"

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
    
    def addRequest(self):
        return f'Request has been added'    
    
    def checkRequest(self):
        return f'Request has been checked'    

    def addUser(self):
        return f'User has been added'   

class TeamLead(Person):
    
    

    def __init__(self, fname, lname, email, dept):


        super().__init__()
        self._fname = fname
        self._lname = lname
        self._email = email
        self._dept = dept
        self._members = []
    
    def getFname(self):
        print(f'The first name of team lead is {self._fname}')
        
    def getLname(self):
        print(f'The last name of team lead is {self._lname}')

    def getEmail(self):
        print(f'The email of team lead is {self._email}')

    def getDept(self):
        print(f'The department of team lead is {self._dept}')

    def setLname(self, lname):
        self._lname = lname
    
    def setFname(self, fname):
        self._fname = fname

    def setEmail(self, email):
        self._email = email
    
    def setDept(self, dept):
        self._dept = dept

    def getFullName(self):
        return f"{self._fname} {self._lname}"
    
    def addRequest(self):
        return f'Request has been added'    
    
    def checkRequest(self):
        return f'Request has been checked'    

    def addUser(self):
        return f'User has been added'    

    def addMember(self, employee):
        self._members.append(employee)

    def getMembers(self):
        return self._members


class Admin(Person):

    def __init__(self, fname, lname, email, dept):

        super().__init__()
        self._fname = fname
        self._lname = lname
        self._email = email
        self._dept = dept
    
    def getFname(self):
        print(f'The first name of admin is {self._fname}')
        
    def getLname(self):
        print(f'The last name of admin is {self._lname}')

    def getEmail(self):
        print(f'The email of admin is {self._email}')

    def getDept(self):
        print(f'The department of admin is {self._dept}')

    def setLname(self, lname):
        self._lname = lname
    
    def setFname(self, fname):
        self._fname = fname

    def setEmail(self, email):
        self._email = email
    
    def setDept(self, dept):
        self._dept = dept

    def getFullName(self):
        return f"{self._fname} {self._lname}"
    
    def addRequest(self):
        return f'Request has been added'    
    
    def checkRequest(self):
        return f'Request has been checked'    

    def addUser(self):
        return f'User has been added'   
        

class Request():

    def __init__(self, name, requester, dateReq):
        self._name = name
        self._requester = requester
        self._dateReq = dateReq
        self._status = 'Open'

    def setName(self, name):
        self._name = name
    
    def setReq(self, requester):
        self._requester = requester

    def setdateReq(self, dateReq):
        self._dateReq = dateReq

    def setStat(self, status):
        self._status = status

    def get_name(self):
        print(f'Request name: {self._name}')
        
    def get_requester(self):
        print(f'Requested by: {self._requester.getFullName()}')

    def get_dateReq(self):
        print(f'Date Requested: {self._dateReq}')

    def get_status(self):
        print(f'Status: {self._status}')

    def updateReq(self, name, requester, dateReq, status):
        self._name = name
        self._name = requester
        self._dateReq = dateReq
        self._status = status

    def closeRequest(self):
        self._status = 'closed'
        return f'{self._name} has been {self._status}'

    def cancelRequest(self):
        self._status = 'cancelled'
        return f'{self._name} has been {self._status}'




employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.setStat("closed")
print(req2.closeRequest())

